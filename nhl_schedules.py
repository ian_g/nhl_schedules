#!/usr/bin/env python3

import os
import json
import requests
import argparse
from datetime import datetime, timedelta

def main():
    #Pull from NHL API
    #Process
    #  Individual team schedules
    #  Giant single schedule
    version = '0.1.2'
    parser = argparse.ArgumentParser(description='Scrape the NHL API to create schedules an NHL team')
    parser.add_argument('--version', '-v', action='store_true', default=False, \
        help='Print version and exit')
    parser.add_argument('--output_dir', '-O', default='.', dest='odir', \
        help='Directory where schedules will be placed')
    parser.add_argument('--csv', action='store_true', default=False, \
        help='Output CSVs instead of JSON')
    parser.add_argument('--season', '-s', metavar='YEAR', dest='season', default=None, \
        help='The season to create schedules for.  Expects the year the season starts')
    #Season - choose from available
    args = parser.parse_args()
  
    if args.version:
        print_version(version)
  
    startdir = os.getcwd()
  
    #Get season selection
    if args.season == None:
        args.season = str(datetime.now().year)
    if not args.season.isdigit():
        print('Argument provided for season is not a digit.  Please supply a four digit year.  Exiting.')
        exit(1)
    if len(args.season) != 4:
        args.season = args.season[:4]
    if int(args.season) < 2011:
        print('This script is not equipped to deal with seasons prior to 2011 (thanks Atlanta).  Sorry :(')
    #Force to upcoming season.  Create parsing for this later
    [start, end] = start_end_dates(args.season)
    print("Downloading NHL schedule data for {}-{} ...".format(args.season, int(args.season)+1))
    season_data = nhl_api_schedule( start, end )
  
    #Parse API content for individual schedules
    #   Home team
    #   Away team
    #   Date/Time
    #   Venue
    teams = ['ANA', 'ARI', 'BOS', 'BUF', 'CAR', 'CBJ', 'CGY', 'CHI', 'COL', 'DAL',
       'DET', 'EDM', 'FLA', 'LAK', 'MIN', 'MTL', 'NJD', 'NSH', 'NYI', 'NYR', 'OTT', 
       'PHI', 'PIT', 'SEA', 'SJS', 'STL', 'TBL', 'TOR', 'VAN', 'WPG', 'WSH', 'VGK']
    if args.odir != '.':
        print("Checking output location ...")
        if os.path.exists(args.odir) and os.path.isdir(args.odir):
            os.chdir(args.odir)
        elif os.path.exists(args.odir) and not os.path.isdir(args.odir):
            print("The path you specified: {} does not exist.  Exiting".format(args.odir))
            exit(1)
        elif not os.path.exists(args.odir):
            os.makedirs(args.odir)
            os.chdir(args.odir)
        else:
            print("??? - Logically shouldn't be possible (line 50)")
            exit(1)
  
    for team in teams:
        print("Creating schedule for {} ...".format(team))
        if args.csv:
            teamsched = single_team_schedule_csv(team, season_data)
            if teamsched == 'Date,Home,Away,Time,Venue':
                continue
            open(team + '.csv', 'w').write(teamsched)
            continue
        teamsched = single_team_schedule_json(team, season_data)
        if teamsched == {}:
            continue #Skip writing a file for teams that didn't play
        open(team + '.json', 'w').write(json.dumps(teamsched, sort_keys=True, indent=2))
  
    #Parse API content for giant schedule
    #   Dict of dates
    #   All teams + games for a date
    #   Home team + Away team
    print("Creating league-wide schedule ...")
    if args.csv:
        big_schedule = all_team_schedule_csv(season_data)
        open('{}_season_schedule.csv'.format(args.season), 'w').write(big_schedule)
    else:
        big_schedule = all_team_schedule_json(season_data)
        open('{}_season_schedule.json'.format(args.season), 'w').write(json.dumps(big_schedule, sort_keys=True, indent=2))
  
    os.chdir(startdir)

def all_team_schedule_json(content):
    #{
    #$date:[
    #    '$AWAY @ $HOME - $TIME', ...
    #  ]
    #}
    sched = {}
    dates = content['dates']
    for date in dates:
        today = []
        games = date['games']
        for game in games:
            if not_regular_season(game):
                continue
            data = game_data(game, 'json')
            today.append("{a} @ {h} - {t} ET".format(h=data['home'],a=data['away'],t=data['time']))
        try:
            sched[data['date']] = today
        except UnboundLocalError:
            #No games that count from a day
            continue
    return sched

def not_regular_season(game):
    if str(game['gamePk'])[4:6] == "02" or game['gameType'] == "R":
        return False
    return True

def all_team_schedule_csv(content):
    #Rows: dates
    #Columns: teams
    #Cells: [@v]-$TEAM
    sched = [
        [
            '', 'ANA', 'ARI', 'BOS', 'BUF', 'CAR', 'CBJ', 'CGY', 'CHI',
            'COL', 'DAL', 'DET', 'EDM', 'FLA', 'LAK', 'MIN', 'MTL', 'NJD', 'NSH', 
            'NYI', 'NYR', 'OTT', 'PHI', 'PIT', 'SEA', 'SJS', 'STL', 'TBL', 'TOR',
            'VAN', 'VGK', 'WPG', 'WSH',
        ]
    ]
    indices = {
        'ANA':1, 'ARI':2, 'BOS':3, 'BUF':4, 'CAR':5, 'CBJ':6, 'CGY':7, 
        'CHI':8, 'COL':9, 'DAL':10, 'DET':11, 'EDM':12, 'FLA':13, 'LAK':14,
        'MIN':15, 'MTL':16, 'NJD':17, 'NSH':18, 'NYI':19, 'NYR':20, 'OTT':21,
        'PHI':22, 'PIT':23, 'SEA': 24, 'SJS':25, 'STL':26, 'TBL':27, 'TOR':28,
        'VAN':29, 'VGK':30, 'WPG':31, 'WSH':32
    }
    dates = content['dates']
    for date in dates:
        today = ['','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','']
        games = date['games']
        for game in games:
            if str(game['gamePk'])[4:][:2] in ['01','03'] or game['gameType'] != 'R':
                continue
            data = game_data(game, 'json')
            today[indices[data['home']]] = 'v_{}'.format(data['away'])
            today[indices[data['away']]] = '@_{}'.format(data['home'])
        try:
            today[0] = data['date']
            sched.append(today)
        except UnboundLocalError:
            #No games count from a day
            continue
    return '\n'.join([",".join(line) for line in sched])

def game_data(game, fmt):
    if fmt not in ['csv', 'json']:
        print("Known formats are 'csv' and 'json'.  Please correct this error")
        exit(1)
    date_time = game['gameDate']
    timestamp = datetime.strptime(date_time, '%Y-%m-%dT%H:%M:%SZ') - timedelta(hours=5) #UTC -> ET
    date = timestamp.strftime('%Y-%m-%d')
    time = timestamp.strftime('%H:%M:%S')
    home = game['teams']['home']['team']['abbreviation']
    away = game['teams']['away']['team']['abbreviation']
    venue = game['venue']['name']
  
    if fmt == 'csv':
        return [date, home, away, time, venue]
    return {'date': date, 'home':home, 'away':away, 'time':time, 'venue':venue}

def single_team_schedule_csv(team, content):
    teamsched = [['Date', 'Home', 'Away', 'Time', 'Venue']]
    dates = content['dates']
    for date in dates:
        games = date['games']
        for game in games:
            if str(game['gamePk'])[4:][:2] in ['01','03'] or game['gameType'] != 'R':
                continue
            data = game_data(game, 'csv')
            if data[1] == team or data[2] == team:
                teamsched.append(data)
    return '\n'.join([",".join(line) for line in teamsched])

def single_team_schedule_json(team, content):
    teamsched = {}
    dates = content['dates']
    for date in dates:
        games = date['games']
        for game in games:
            if str(game['gamePk'])[4:][:2] in ['01','03'] or game['gameType'] != 'R':
                continue
            data = game_data(game, 'json')
            if data['home'] == team or data['away'] == team:
                date = data['date']
                del(data['date'])
                teamsched[date] = data
    return teamsched

def start_end_dates(season):
    season = season[:4]
    end = str(int(season)+1)
    start_date = '09/15/{}'.format(season)
    end_date = '06/01/{}'.format(end)
    return [start_date, end_date] 

def nhl_api_schedule(start, end):
    url = 'https://statsapi.web.nhl.com/api/v1/schedule?startDate={sdate}&endDate={edate}&expand=schedule.teams,schedule.linescore'
    page = requests.get(url.format(sdate=start,edate=end))
    content = page.json()
    page.close()
    return content

def print_version(version):
    print(
    'NHL Schedule Creator\
    \n  Version: {}\
    \n  Source: https://gitlab.com/ian_g/nhl_schedules'\
    .format(version)
    )
    exit(0)

if __name__ == '__main__':
    main()
