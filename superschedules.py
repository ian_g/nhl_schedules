#!/usr/bin/env python3

import os
import json
import argparse
from datetime import datetime, timedelta

def main():
  
    version = '0.1.1'
    parser = argparse.ArgumentParser(description='Superschedule creator for NHL teams.  Requires json-formatted schedule (nhl_schedules.py --output_dir json)')
    parser.add_argument('--version', '-v', action='store_true', default=False,\
        help='Print version and exit')
    parser.add_argument('--csv', action='store_true', default=False,\
        help='Output CSVs instead of JSON')
    parser.add_argument('--input_dir', '-i', default='.', dest='idir',\
        help='Directory where schedules are looked for.  Defaults to cwd')
    parser.add_argument('--distances', '-d', default='distances.json', dest='dist',\
        help='Location of distances file.  Defaults to ./distances.json')
    parser.add_argument('--output_dir', '-o', default='.', dest='odir',\
        help='Directory where superschedules will be placed.  Defaults to cwd')
    args = parser.parse_args()
  
    if args.version:
        print_version(version)
    
    schedules = read_schedules(args.idir)
    distances = load_distances(args.dist)
  
    create_output(args.odir)
  
    summary = {} #Avoid a second loop through and set of calculations.  Marked with #SUMMARY#
    for team in list(schedules):
        print("Analyzing schedule for {}".format(team))  
        total_distance = 0 #SUMMARY#
        back_2_backs = 0   #SUMMARY#
    
        sched = schedules[team]
        prev_home = team
        prev_date = '2000-01-01' #First game aint b2b
        supersched = {}
    
        #Dictionary - default unsorted
        #Sort so don't have games out of order
        gamedates = list(sched)
        gamedates.sort()
    
        for date in gamedates:
            supersched[date] = sched[date]
            next_home = sched[date]['home']
            distance_to = distance_traveled(prev_home, next_home, distances)
            supersched[date]['distance_traveled'] = distance_to
            supersched[date]['B2B'] = False
            if is_b2b(prev_date, date):
                supersched[date]['B2B'] = True
                back_2_backs += 1  #SUMMARY#
            prev_home = sched[date]['home']
            prev_date = date
      
            total_distance += int(distance_to) #SUMMARY#
    
        summary[team] = { 'Travel': total_distance, 'B2Bs': back_2_backs } #SUMMARY#
    
        if args.csv:
            csvfile = build_csv_superschedule(sched, gamedates)
            open(os.path.join(args.odir, team+'.super.csv'), 'w').write(csvfile)
        else:
            open(os.path.join(args.odir, team+'.super.json'), 'w').write(json.dumps(supersched, indent=2, sort_keys=True))
  
    #Writing SUMMARY to file#
    print("Writing summary data to file ...")
    if args.csv:
        csvfile = build_csv_summary(summary)
        open(os.path.join(args.odir, 'supersched.summary.csv'), 'w').write(csvfile)
    else:
        open(os.path.join(args.odir, 'supersched.summary.json'), 'w').write(json.dumps(summary, indent=2, sort_keys=True))

def build_csv_summary(summary):
    summarycsv = [['Team','Travel Miles','Back to Backs']]
    for team in summary:
        summarycsv.append([ team, str(summary[team]['Travel']), str(summary[team]['B2Bs']) ])
    summarycsv = '\n'.join([','.join(line) for line in summarycsv])
    return summarycsv

def build_csv_superschedule(schedule, ordered):
    csvsched = [ [ 'Date', 'Home', 'Away', 'Venue', 'Distance Traveled', 'B2B' ] ]
    for date in ordered: 
        game = schedule[date]
        csvsched.append([ date, game['home'], game['away'], game['venue'], game['distance_traveled'], str(game['B2B']) ])
    csvsched = '\n'.join([','.join(line) for line in csvsched])
    return csvsched

def create_output(locn):
    if os.path.exists(locn) and os.path.isdir(locn):
        print('Superschedule location exists. Continuing.')
    elif os.path.exists(locn) and not os.path.isdir(locn):
        print('Output location exists but is not a directory.  Exiting')
        exit(1)
    else:
        print('Creating output directory')
        try:
            os.mkdir(locn)
        except FileNotFoundError as e:
            print('Problem creating directory.  Are you trying to create multiple nested directories?')
            exit(1)

def is_b2b(then, now):
    then = datetime.strptime(then, '%Y-%m-%d')
    now = datetime.strptime(now, '%Y-%m-%d')
    if (now - then).days > 1:
        return False
    return True 
      
def distance_traveled(prev, prox, distances):
    return distances[prev][prox]

def load_distances(dist):
    if not os.path.exists(dist) or not os.path.isfile(dist):
        print('The provided distances file either does not exist or is not a file.  Exiting.')
        exit(0)

    #If not json, fails with error here.  Thanks json package
    distances = json.loads(open(dist).read())
    return distances

def read_schedules(locn):
    if not os.path.exists(locn) or not os.path.isdir(locn):
        print('The provided input directory either does not exist or is not a directory.  Exiting.')
        exit(0)

    schedules = {}
    startdir = os.getcwd()
    os.chdir(locn)
    files = [ item for item in os.listdir('.') if len(item) == 8 and item[3:] == '.json' ]
    for sched in files:
        team = sched[:3]
        contents = json.loads(open(sched).read())
        schedules[team] = contents
    
    os.chdir(startdir)
    return schedules

def print_version(version):
    print(
        'NHL Superschedule Calculator (Distance Traveled and B2Bs)\
        \n  Version: {}\
        \n  Source: https://gitlab.com/ian_g/nhl_schedules'\
        .format(version)
    )  
    exit(0)
 
if __name__ == '__main__':
    main()
